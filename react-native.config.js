module.exports = {
  dependencies: {
    'react-native-splash-screen': {
      platforms: {
        ios: null, // disable Android platform, other platforms will still autolink if provided
      },
    },
  },
};
