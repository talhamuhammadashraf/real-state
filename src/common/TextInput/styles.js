import { StyleSheet } from 'react-native';
import { Colors, Metrics } from '../../theme';

export default StyleSheet.create({
  input: {
    width: '80%',
    marginVertical: Metrics.smallMargin,
    backgroundColor: Colors.backgroundPrimary,
  },
});
