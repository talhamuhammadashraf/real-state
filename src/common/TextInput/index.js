import React from 'react';
import { TextInput } from 'react-native-paper';

import { Colors } from '../../theme';
import styles from './styles';

export default function ({ forwardedRef, ...rest }) {
  return (
    <TextInput
      // returnKeyType="default"
      theme={{ colors: { primary: Colors.primary } }}
      mode="outlined"
      style={styles.input}
      ref={forwardedRef}
      {...rest}
    />
  );
}
