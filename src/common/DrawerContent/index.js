import React, { useEffect } from 'react';
import { useDrawerStatus } from '@react-navigation/drawer';
import {
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { Colors } from '../../theme';
import { Util } from '../../utils';
import styles from './styles';

const TiggoleStatusBar = () => {
  useEffect(() => {
    console.log('open');
    return () => console.log('close');
  }, []);
  const isDrawerOpen = useDrawerStatus() === 'open';
  console.log({ isDrawerOpen });
  return null;
};

const CustomDrawerContent = props => {
  const onPressLogout = () =>
    Util.showAlertConfirm(
      'Are you sure, You want to logout?',
      undefined,
      'Logout',
      Util.DoNothing
    );

  return [
    <TiggoleStatusBar />,

    <DrawerContentScrollView {...props} scrollEnabled={false}>
      {/* <Image
        source={require('../../assets/talha-teams.jpg')}
        style={styles.profileImage}
      /> */}
      <FontAwesomeIcon
        name="user"
        color={Colors.primary}
        size={80}
        style={styles.profileIcon}
      />
      <DrawerItemList {...props} />

      <DrawerItem
        icon={() => (
          <MaterialCommunityIcons
            name="logout"
            color={Colors.primary}
            size={20}
          />
        )}
        onPress={onPressLogout}
        label="Logout"
        inactiveTintColor={Colors.primary}
      />
    </DrawerContentScrollView>,
  ];
};

export default CustomDrawerContent;
