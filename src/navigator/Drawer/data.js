//remove later
import React from 'react';
import { Text } from 'react-native';
//remove later

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import { EditProfile, MarketTrends } from '../../containers';

export default [
  {
    name: 'EditProfile',
    title: 'Edit Profile',
    iconFamily: MaterialCommunityIcons,
    iconName: 'account-edit',
    component: EditProfile,
  },
  {
    name: 'History',
    title: 'History',
    iconFamily: FontAwesome,
    iconName: 'history',
    component: EditProfile,
  },
  {
    name: 'AddProperty',
    title: 'Add Property',
    iconFamily: MaterialIcons,
    iconName: 'add-business',
    component: EditProfile,
  },
  {
    name: 'MySubmission',
    title: 'My Submission',
    iconFamily: Entypo,
    iconName: 'text-document',
    component: EditProfile,
  },
  {
    name: 'MarketTrends',
    title: 'Market Trends',
    iconFamily: FontAwesome,
    iconName: 'line-chart',
    component: MarketTrends,
  },
  //   {
  //     name: 'Logout',
  //     title: 'Logout',
  //     iconFamily: MaterialIcons,
  //     iconName: 'logout',
  //   },
];
