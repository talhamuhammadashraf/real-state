import { StyleSheet } from 'react-native';
import { Colors, Metrics } from '../../theme';
export default StyleSheet.create({
  profileImage: {
    height: 80,
    width: 80,
    borderRadius: 40,
    borderWidth: 2,
    borderColor: Colors.primary,
    alignSelf: 'center',
    marginVertical: Metrics.baseMargin,
  },
});
