import React, { useLayoutEffect } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';

import CustomDrawerContent from '../../common/DrawerContent';
import { Colors } from '../../theme';
import { drawerOptions } from '../config';
import DrawerScreens from './data';

const Drawer = createDrawerNavigator();

export default ({ navigation }) => {
  useLayoutEffect(() => {
    navigation.setOptions({ headerShown: false });
  }, [navigation]);

  return (
    <Drawer.Navigator
      screenOptions={drawerOptions}
      backBehavior="history"
      drawerContent={CustomDrawerContent}
    >
      {DrawerScreens.map(
        ({ name, title, iconName, iconFamily: Icon, component }, key) => (
          <Drawer.Screen
            {...{ name, key, component }}
            options={{
              title,
              drawerIcon: () => (
                <Icon name={iconName} color={Colors.primary} size={20} />
              ),
            }}
          />
        )
      )}
    </Drawer.Navigator>
  );
};
