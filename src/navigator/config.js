import * as React from 'react';

// import styles from './styles';
import { Text } from '../components';
import { Colors, Metrics } from '../theme';

export const screenOptions = () => ({
  headerBackTitleVisible: false,
  headerTitleAlign: 'center',
  // headerTitleStyle: AppStyles.headerTitleStyle,
});

export const modalOptions = () => ({
  headerBackTitleVisible: false,
  headerTitleAlign: 'center',
});

export const modalStackOptions = {
  headerShown: false,
  gestureEnabled: false,
};

export const hideHeaderOptions = {
  headerShown: false,
};

export const transitionSpecSearch = {
  // close: TransitionSpecs.FadeOutToBottomAndroidSpec,
  // open: TransitionSpecs.FadeInFromBottomAndroidSpec,
};

export const drawerOptions = {
  drawerHideStatusBarOnOpen: false,
  drawerPosition: 'left',
  keyboardDismissMode: 'on-drag',
  drawerStyle: { width: '70%', backgroundColor: Colors.white },
  drawerActiveTintColor: Colors.primary,
  drawerInactiveTintColor: Colors.primary,
  drawerLabelStyle: { fontSize: Metrics.ratio(12), fontStyle: 'italic' },
  headerTintColor: Colors.white,
  unmountOnBlur: true,
  // headerTitleStyle:
  // headerBackgroundContainerStyle: { backgroundColor: Colors.primary },
  headerStyle: { backgroundColor: Colors.primary },
};
