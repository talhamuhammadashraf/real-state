import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {
  createNativeStackNavigator,
  // TransitionPresets,
} from '@react-navigation/native-stack';
import { enableScreens } from 'react-native-screens';

import { Login } from '../containers';

// import { NavigationService } from '../utils';
import {
  screenOptions,
  modalOptions,
  // transitionSpecSearch,
  // modalStackOptions,
  // hideHeaderOptions,
  // transitionSpecCall,
} from './config';
import Drawer from './Drawer';
import { Text } from 'react-native';
import { Colors } from '../theme';
// import { useSelector } from 'react-redux';

enableScreens();

const RootStack = createNativeStackNavigator();
const Stack = createNativeStackNavigator();

const AppStack = () => {
  // const userExists = false; //useSelector(isGuest) ? false : true;
  const initialRouteName = 'Dashboard';
  return (
    <Stack.Navigator {...{ screenOptions, initialRouteName }}>
      {/* <Stack.Screen name="Dashboard" component={() => <Text>hi</Text>} /> */}
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Dashboard" component={Drawer} />
    </Stack.Navigator>
  );
};

const App = () => {
  const LightTheme = {
    colors: { primary: Colors.primary, underlineColor: 'transparent' },
  };

  return (
    <NavigationContainer
    // theme={{ dark: true }}
    // ref={navigatorRef => {
    //   NavigationService.setTopLevelNavigator(navigatorRef);
    // }}
    >
      <RootStack.Navigator mode="modal" screenOptions={modalOptions}>
        <RootStack.Screen
          name="Main"
          component={AppStack}
          options={{
            headerShown: false,
            animationEnabled: false,
            // ...TransitionPresets.SlideFromRightIOS,
          }}
        />
        {/* <RootStack.Screen
          name="SearchScreen"
          component={SearchScreen}
          options={{ transitionSpec: transitionSpecSearch }}
        /> */}
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default App;
