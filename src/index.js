import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import SplashScreen from 'react-native-splash-screen';
import AppNavigator from './navigator';
import { StatusBar } from 'react-native';
import { Colors } from './theme/index';
import { Util } from './utils';

const App = () => {
  useEffect(() => {
    if (Util.isPlatformAndroid()) {
      SplashScreen.hide();
    }
  }, []);

  return (
    <>
      <StatusBar backgroundColor={Colors.primary} barStyle="light-content" />
      <AppNavigator />
    </>
  );
};

export default App;
