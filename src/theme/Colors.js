export default {
  white: '#ffffff',
  black: '#000',
  placeholder: '#b2b2b2',
  primary: '#76A6F0',
  primary05: 'rgba(118,166,240,0.7)',
  backgroundSecDark: '#292929',
  backgroundSec: '#ffffff',
  backgroundPrimary: '#ffffff',
  backgroundPrimaryDark: '#000000',

  textPrimaryDark: '#ffffff',
  textPrimary: '#000000',

  lightPink: '#F9CEDF',
};
