// // @flow
import { StyleSheet } from 'react-native';
import { Metrics, Colors } from '../theme';

export default StyleSheet.create({
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  flex1: {
    flex: 1,
  },
  scrollContainer: {
    backgroundColor: Colors.white,
    paddingBottom: Metrics.BOTTOM_SPACING,
  },
  contentContainerStyle: { paddingBottom: Metrics.mediumMargin },
  contentContainerStyle2: { paddingBottom: Metrics.BOTTOM_SPACING },
  backgroundColor: { backgroundColor: Colors.white },
  container: {
    backgroundColor: Colors.white,
    flex: 1,
    paddingHorizontal: Metrics.mediumMargin,
  },
  headerSpace: {
    paddingTop: Metrics.navBarHeight,
    backgroundColor: Colors.white,
  },
  spreadRow: { flexDirection: 'row', justifyContent: 'space-between' },
  spreadRowAligned: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  spreadRowEnd: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  alignCenterView: { justifyContent: 'center', alignItems: 'center' },
  spreadRowStart: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  row: { flexDirection: 'row' },
  rowAligned: { flexDirection: 'row', alignItems: 'center' },
  flatlist: {
    backgroundColor: Colors.white,
  },
  headerStyle: {
    elevation: 0,
    shadowOpacity: 0,
    backgroundColor: Colors.white,
  },
});
