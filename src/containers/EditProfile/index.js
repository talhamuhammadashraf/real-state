import React, { useLayoutEffect, useRef } from 'react';
import { Image, ScrollView, Text, TouchableOpacity, View } from 'react-native';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';

import { TextInput as TextField } from '../../common';
import { AppStyles, Colors } from '../../theme';
import { Util } from '../../utils';
import styles from './styles';

const ProfileCirlce = () => (
  <View style={styles.profileCircle}>
    <Image
      source={require('../../assets/talha-teams.jpg')}
      style={styles.profileImage}
    />
    {/* <FontAwesomeIcon name="user" color={Colors.primary05} size={100} /> */}
    <View style={styles.editPhoto}>
      <Text style={{ fontWeight: '400', color: Colors.white, fontSize: 17 }}>
        Edit Photo
      </Text>
    </View>
  </View>
);

export default function ({ navigation }) {
  const contactNumberRef = useRef(),
    userNameRef = useRef();

  useLayoutEffect(() => {
    navigation.setOptions({
      headerTitle: '',
      headerRight: ({ tintColor: color }) => (
        <TouchableOpacity onPress={() => alert('hi')}>
          <Text style={[{ color }, styles.headerRightText]}>Save</Text>
        </TouchableOpacity>
      ),
    });
  }, [navigation]);

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={AppStyles.scrollContainer}
    >
      <ProfileCirlce />

      <TextField
        label="Agent Full Name"
        placeholder="John Doe"
        style={styles.input}
        returnKeyType="next"
        onSubmitEditing={() => contactNumberRef.current.focus()}
      />

      <TextField
        forwardedRef={contactNumberRef}
        label="Agent Contact Number"
        placeholder="0321 1234567"
        style={styles.input}
        returnKeyType="next"
        keyboardType="number-pad"
        onSubmitEditing={() => userNameRef.current.focus()}
      />

      <TextField
        forwardedRef={userNameRef}
        label="Username"
        autoCapitalize="none"
        placeholder="johndoe"
        style={styles.input}
        blurOnSubmit
      />

      <TouchableOpacity onPress={Util.DoNothing}>
        <TextField
          pointerEvents="none"
          editable={false}
          label="Password"
          style={styles.input}
        />
      </TouchableOpacity>
    </ScrollView>
  );
}
