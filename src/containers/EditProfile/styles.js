import { StyleSheet } from 'react-native';
import { Colors, Metrics } from '../../theme';

export default StyleSheet.create({
  input: {
    width: '88%',
    marginVertical: Metrics.smallMargin,
    backgroundColor: Colors.backgroundPrimary,
    alignSelf: 'center',
  },
  profileCircle: {
    height: 120,
    width: 120,
    borderRadius: 60,
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.primary,
    alignSelf: 'center',
    marginTop: Metrics.scaleVertical(30),
    marginBottom: Metrics.scaleVertical(20),
  },
  container: {
    backgroundColor: Colors.white,
    paddingBottom: Metrics.BOTTOM_SPACING,
  },
  headerRightText: { marginRight: 20, fontSize: 15 },
  profileImage: {
    height: 116,
    width: 116,
    borderRadius: 58,
  },
  editPhoto: {
    height: 60,
    width: 120,
    transform: [{ scaleY: 0.857 }, { scaleX: 0.9675 }],
    borderBottomRightRadius: 60,
    borderBottomLeftRadius: 60,
    backgroundColor: Colors.primary05,
    position: 'absolute',
    bottom: -5,
    zIndex: 2,
    paddingTop: 12,
    alignItems: 'center',
  },
});
