import React, { useLayoutEffect, useRef } from 'react';
import { View } from 'react-native';
import { Button, TextInput } from 'react-native-paper';

import { TextInput as TextField } from '../../common';
import { Colors, Metrics } from '../../theme';
import styles from './styles';
export default function ({ navigation }) {
  const passwordRef = useRef();

  useLayoutEffect(() => {
    navigation.setOptions({ headerShown: false });
  }, [navigation]);

  const onPressLogin = () => navigation.navigate('Dashboard');
  return (
    <View style={styles.container}>
      <TextField
        // theme={{ colors: { primary: Colors.primary } }}
        // mode="outlined"
        label="Email"
        placeholder="abc@example.com"
        style={styles.input}
        keyboardType="email-address"
        returnKeyType="next"
        returnKeyLabel="next"
        onSubmitEditing={() => passwordRef.current.focus()}
      />

      <TextField
        forwardedRef={passwordRef}
        maxLength={15}
        // theme={{ colors: { primary: Colors.primary } }}
        // mode="outlined"
        label="Password"
        placeholder="********"
        secureTextEntry
        right={
          <TextInput.Icon
            name="eye"
            color={isFocused =>
              isFocused ? Colors.primary : Colors.placeholder
            }
          />
        }
        style={styles.input}
        returnKeyType="done"
        returnKeyLabel="login"
        onSubmitEditing={onPressLogin}
        blurOnSubmit
      />

      <Button
        labelStyle={{ color: Colors.white }}
        theme={{ colors: { primary: Colors.primary } }}
        mode="contained"
        onPress={onPressLogin}
        style={{ width: '80%', marginTop: Metrics.smallMargin }}
      >
        Login
      </Button>
    </View>
  );
}
