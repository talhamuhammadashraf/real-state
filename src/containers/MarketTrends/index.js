import React from 'react';
import { ScrollView } from 'react-native';
import { VictoryChart, VictoryLine } from 'victory-native';
import { List } from 'react-native-paper';

import { AppStyles, Colors } from '../../theme';

export default function () {
  const data = [
    { x: 1, y: 2 },
    { x: 2, y: 3 },
    { x: 3, y: 5 },
    { x: 4, y: 4 },
    { x: 5, y: 7 },
  ];
  //   const [expanded, setExpanded] = React.useState(true);

  //   const handlePress = () => setExpanded(!expanded);
  return (
    <ScrollView style={AppStyles.scrollContainer}>
      <List.Section title="Accordions">
        <List.Accordion title="Uncontrolled Accordion">
          <List.Item title="First item" />
          <List.Item title="Second item" />
        </List.Accordion>

        <List.Accordion
          title="Controlled Accordion"
          //   expanded={expanded}
          //   onPress={handlePress}
        >
          <List.Item title="First item" />
          <List.Item title="Second item" />
        </List.Accordion>
      </List.Section>

      <VictoryChart style={{ background: { fill: Colors.white } }}>
        <VictoryLine
          style={{ data: { stroke: Colors.primary } }}
          {...{ data }}
        />
      </VictoryChart>
    </ScrollView>
  );
}
