import { Platform, StatusBar, Alert } from 'react-native';
import { MessageBarManager } from 'react-native-message-bar';
// import { normalize, schema } from 'normalizr';
import moment from 'moment';
import _ from 'lodash';

import { Colors } from '../theme';

function isPlatformAndroid() {
  return Platform.OS === 'android';
}

function isPlatformIOS() {
  return Platform.OS === 'ios';
}

function getPlatform() {
  return Platform.OS;
}

function isEmpty(data) {
  return _.isEmpty(data);
}

function formatDate(dateString, currentDateFormat, formattedDateFormat) {
  return dateString
    ? moment(dateString, currentDateFormat).format(formattedDateFormat)
    : '';
}

function showMessage(message, alertType = 'error', duration: 5000) {
  MessageBarManager.showAlert({
    message,
    alertType,
    duration,
    stylesheetInfo: { backgroundColor: Colors.primary },
    messageStyle: { color: Colors.white },
  });
}

function hideMessageBar() {
  try {
    MessageBarManager.hideAlert();
  } catch (error) {}
}

function createSlug(text: String) {
  return text?.trim().toLocaleLowerCase().split(' ').join('_');
}

function DoNothing() {
  return null;
}

function alterObjKeys(obj, replacingKeys) {
  let myObj = { ...obj };
  for (let key in myObj) {
    if (replacingKeys.hasOwnProperty(key)) {
      myObj[replacingKeys[key]] = myObj[key];
      delete myObj[key];
    }
  }
  return myObj;
}

function isNotEmpty(data) {
  return !_.isEmpty(data, true);
}

function clone(data) {
  return _.clone(data);
}

function cloneDeep(data) {
  return _.cloneDeep(data);
}

function compareDeep(previous, next) {
  return !_.isEqual(previous, next);
}

function toFixedIfNecessary(value, dp = 1) {
  return +parseFloat(value).toFixed(dp);
}

function setStatusBarStyle(barStyle, color, animated = true) {
  StatusBar.setBarStyle(barStyle, animated);
  StatusBar.setBackgroundColor(color, animated);
}

function showAlertConfirm(
  title,
  message,
  doneText,
  onDonePress,
  cancelText = 'Cancel'
) {
  Alert.alert(
    title,
    message,
    [
      {
        text: cancelText,
        onPress: () => {},
        style: 'cancel',
      },
      { text: doneText, onPress: () => onDonePress() },
    ],
    { cancelable: true }
  );
  //StatusBar.setBarStyle(barStyle, true);
}

// function normalizeData(data, id = '_id') {
//   const offerSchema = new schema.Entity('listItems', {}, { idAttribute: id });
//   const offerListSchema = [offerSchema];
//   const normalizedData = normalize(data, offerListSchema);
//   return {
//     ids: normalizedData.result,
//     items: normalizedData.entities.listItems || {},
//   };
// }

function getDateFromNow(date) {
  return date ? moment(date).fromNow() : '';
}

function translucentApp() {
  if (isPlatformAndroid()) {
    StatusBar.setTranslucent(true);
    StatusBar.setBarStyle('dark-content');
  }
}

export default {
  translucentApp,
  showMessage,
  setStatusBarStyle,
  isPlatformAndroid,
  isPlatformIOS,
  isEmpty,
  getPlatform,
  getDateFromNow,
  isNotEmpty,
  clone,
  cloneDeep,
  compareDeep,
  formatDate,
  createSlug,
  DoNothing,
  showAlertConfirm,
  alterObjKeys,
  toFixedIfNecessary,
};
